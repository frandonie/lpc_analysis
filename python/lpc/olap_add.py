import numpy as np

def olap_add(B, w, R = 0.5):
    [count, nw] = B.shape
    step = int(np.floor(nw * R))

    n = (count-1) * step + nw

    x = np.zeros((n, ))

    for i in range(count):
        offset = i * step
        x[offset : nw + offset] += B[i, :]

    return x
