import numpy as np
from slice import slice
from solve import solve

def encode(x, p, w):
    B = slice(x, w)
    [nb, nw] = B.shape

    A = np.zeros((p, nb))
    G = np.zeros((1, nb))

    for i in range(nb):
        [a, g] = solve(B[i, :], p, i)

        A[:, i] = a
        G[:, i] = g

    return [A, G]
