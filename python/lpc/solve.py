import numpy as np
from make_matrix import make_matrix

def solve(x, p, ii):
    b = x[1:]

    X = make_matrix(x, p)

    a = np.linalg.lstsq(X, b.T, rcond=None)[0]

    e = b - np.dot(X, a)
    g = np.var(e)

    return [a, g]
