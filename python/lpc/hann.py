import scipy.signal.windows as ssw

def hann(size):
	sym = False # we'll see about *that*, I think a symmetrical window has better (linear) phase response
	return ssw.hann(int(size), sym)
