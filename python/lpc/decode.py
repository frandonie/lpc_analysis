import numpy as np
from noise_excitation import noise_excitation
from olap_add import olap_add

def decode(A, G, w, lowcut = 0):

    [ne, n] = G.shape
    nw = len(w)
    [p, _] = A.shape

    B_hat = np.zeros((n, nw))

    for i in range(n):
        B_hat[i,:] = noise_excitation(A[:, i], G[:, i], nw)

    # recover signal from blocks
    x_hat = olap_add(B_hat, w);

    return x_hat
