import numpy as np

def slice(x, w, R = 0.5):
    n = len(x)
    nw = len(w)
    step = int(np.floor(nw * (1 - R)))
    nb = int(np.floor((n - nw) / step) + 1)

    B = np.zeros((nb, nw))

    for i in range(nb):
        offset = i * step
        B[i, :] = w * x[offset : nw + offset]

    return B
