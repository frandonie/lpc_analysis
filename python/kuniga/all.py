#
# Kuniga's code revisited
#
# The original can be found here:
#
# https://www.kuniga.me/blog/2021/05/13/lpc-in-python.html
#
import os.path, sys
sys.path.append('..')
sys.path.append(os.path.join('..', 'lpc'))

import numpy as np
import scipy.io.wavfile as siw
import lpc

ex_path = os.path.join('..', '..', 'sounds')

(sample_rate, signal) = lpc.reader(os.path.join(ex_path, 'speech.wav'))

# resampling to 8kHz
target_sample_rate = 8000
(sample_rate, signal) = lpc.resample(signal, target_sample_rate, sample_rate)

# 30ms Hann window
w = lpc.hann(np.floor(0.03*sample_rate))

# Encode
p = 6 # number of poles
[A, G] = lpc.encode(signal, p, w)

# Print stats
original_size = len(signal)
model_size = A.size + G.size
print('Original signal size:', original_size)
print('Encoded signal size:', model_size)
print('Data reduction:', original_size/model_size)

xhat = lpc.decode(A, G, w)

siw.write("example_out.wav", sample_rate, xhat)
print('done')
